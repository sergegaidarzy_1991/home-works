public class Main {
    public static void main(String[] args) {
//        findAverage();
//        justCreateArray
//        findMaxNumber();

    }
    public static void findAverage() {
        int j = 1;
        int sum = 0;
        for (int i = 100; i >= 51; i--) {
            sum = sum + (i+j);
            j++;
        }
        float average = (float) sum / 100;
        System.out.println("Среднее значение: " + average);
    }

    public static void justCreateArray() {
        int [] array = new int [10];
        array[0] = 23;
        array[1] = 45;
        array[2] = 67;
        array[3] = 623;
        array[4] = 2;
        array[5] = 256;
        array[6] = 89;
        array[7] = 78;
        array[8] = 222;
        array[9] = 345;
    }

    public static void findMaxNumber() {
        int [] array = {5,2,4,8,88,22,10};
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if ( max< array[i]) {
                max = array[i];
            }
        }
        System.out.println("Результат: " + max);
    }
}
