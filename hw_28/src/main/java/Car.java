public class Car {
    public float gasTank = 100f;
    public float speed = 0f;
    public String mark = "";


    public void drive(int speed, int km){
        if (this.gasTank > 5) {
            this.speed = speed;

                for (int i = 1; i <= km; i++) {
                    this.gasTank-= 0.5f;
                }

            System.out.println("Машина по имени " + this.mark + " едет с скоростью " + speed);
            System.out.println("У машины " + this.mark + " осталось " + this.gasTank + " бензина, проехала " + km + " км");
        } else if (this.gasTank <= 5 ) {
            System.out.println("В машине очень мало бензина и она не может выехать");
        }
    }
}
